# Jelonki
This is a part of project called *Jelonki* that aims at being a simulator of wild animals, mainly focused on their behaviour, movement, interaction with different species.
## This is just a GUI
Core functionality is handled by a *back-end* - program written in Rust, maintained by Maciej Zwoliński:
[Repo is here](https://gitlab.com/Zwo1in/jelonki-animus)
## How does it work
Simulator is designed as two standalone programs. Communication is done by streams (stdin, stdout for data, stderr for logging).
This program can both join to existing and running back-end process or launch and maintain its lifetime by itself.
After handshake via stdout/stdin indicating valid connection data is translated into JSON-strings that are sent with frequency specified by the user.
## What's TODO
1. Asynchronous interface that does not run once per frame.
2. Animal despawning.
3. More UTs covering game engine functionality and its interface with the 'program'.
4. Terrain features, like water and grass
