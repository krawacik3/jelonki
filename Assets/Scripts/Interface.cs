﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Threading;
using UnityEngine;
using Newtonsoft.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEditor.Experimental;
using Debug = UnityEngine.Debug;


//JSON objects
public class JsonPosition
{
    public double x { get; set; }
    public double y { get; set; }
}

class JsonAnimal
{
    public int id { get; set; }
    public string kind { get; set; }
    public JsonPosition position { get; set; }
    public JsonPosition velocity { get; set; }
}

class JsonMap
{
    public JsonPosition[] grass { get; set; }
    public JsonPosition[] water { get; set; }
    public JsonPosition[] trees { get; set; }
}
class JsonWorld
{
    public JsonAnimal[] animals { get; set; }
    public JsonMap map { get; set; }
}
class JsonSignIn
{
    public int physix_rate { get; set; }
    public int update_rate { get; set; }
    public JsonWorld world { get; set; }
}

class JsonUpdateAnimals
{
    public JsonAnimal[] animals { get; set; }
}

public class ConsoleAppManager
{
    private readonly string appName;
    private readonly Process process = new Process();
    private readonly object theLock = new object();
    private SynchronizationContext context;
    private string pendingWriteData;

    public ConsoleAppManager(string appName)
    {
        this.appName = appName;

        this.process.StartInfo.FileName = this.appName;
        this.process.StartInfo.RedirectStandardError = true;
        this.process.StartInfo.StandardErrorEncoding = Encoding.UTF8;

        this.process.StartInfo.RedirectStandardInput = true;
        this.process.StartInfo.RedirectStandardOutput = true;
        this.process.EnableRaisingEvents = true;
        this.process.StartInfo.CreateNoWindow = true;

        this.process.StartInfo.UseShellExecute = false;

        this.process.StartInfo.StandardOutputEncoding = Encoding.UTF8;

        this.process.Exited += this.ProcessOnExited;
    }

    public event EventHandler<string> ErrorTextReceived;
    public event EventHandler ProcessExited;
    public event EventHandler<string> StandardTextReceived;

    public int ExitCode
    {
        get { return this.process.ExitCode; }
    }

    public bool Running
    {
        get; private set;
    }

    public void ExecuteAsync(params string[] args)
    {
        if (this.Running)
        {
            throw new InvalidOperationException(
                "Process is still Running. Please wait for the process to complete.");
        }

        string arguments = string.Join(" ", args);

        this.process.StartInfo.Arguments = arguments;

        this.context = SynchronizationContext.Current;

        this.process.Start();
        this.Running = true;

        new Task(this.ReadOutputAsync).Start();
        new Task(this.WriteInputTask).Start();
        new Task(this.ReadOutputErrorAsync).Start();
    }

    public void Write(string data)
    {
        if (data == null)
        {
            return;
        }

        lock (this.theLock)
        {
            this.pendingWriteData = data;
        }
    }

    public void Kill()
    {
        if (Running)
        {
            this.process.Kill();
        }
    }

    public void WriteLine(string data)
    {
        this.Write(data + Environment.NewLine);
    }

    protected virtual void OnErrorTextReceived(string e)
    {
        EventHandler<string> handler = this.ErrorTextReceived;

        if (handler != null)
        {
            if (this.context != null)
            {
                this.context.Post(delegate { handler(this, e); }, null);
            }
            else
            {
                handler(this, e);
            }
        }
    }

    protected virtual void OnProcessExited()
    {
        EventHandler handler = this.ProcessExited;
        if (handler != null)
        {
            handler(this, EventArgs.Empty);
        }
    }

    protected virtual void OnStandartTextReceived(string e)
    {
        EventHandler<string> handler = this.StandardTextReceived;

        if (handler != null)
        {
            if (this.context != null)
            {
                this.context.Post(delegate { handler(this, e); }, null);
            }
            else
            {
                handler(this, e);
            }
        }
    }

    private void ProcessOnExited(object sender, EventArgs eventArgs)
    {
        this.OnProcessExited();
    }

    private async void ReadOutputAsync()
    {
        var standart = new StringBuilder();
        var buff = new char[100000];
        int length;

        while (this.process.HasExited == false)
        {
            standart.Clear();

            length = await this.process.StandardOutput.ReadAsync(buff, 0, buff.Length);
            standart.Append(buff.Take(length).ToArray());
            this.OnStandartTextReceived(standart.ToString());
            Thread.Sleep(1);
        }

        this.Running = false;
    }

    private async void ReadOutputErrorAsync()
    {
        var sb = new StringBuilder();

        do
        {
            sb.Clear();
            var buff = new char[100000];
            int length = await this.process.StandardError.ReadAsync(buff, 0, buff.Length);
            sb.Append(buff.Take(length).ToArray());
            this.OnErrorTextReceived(sb.ToString());
            Thread.Sleep(1);
        }
        while (this.process.HasExited == false);
    }

    private async void WriteInputTask()
    {
        while (this.process.HasExited == false)
        {
            Thread.Sleep(1);

            if (this.pendingWriteData != null)
            {
                await this.process.StandardInput.WriteLineAsync(this.pendingWriteData);
                await this.process.StandardInput.FlushAsync();

                lock (this.theLock)
                {
                    this.pendingWriteData = null;
                }
            }
        }
    }
}

public class Interface : MonoBehaviour
{
    // Back-end process
    private ConsoleAppManager consoleAppManager;
    public string[] backendResponses = new string[] {};
    public bool initialized = false;
    public bool started = false;

    private string fileNameString = "/home/krawacik3/rust/jelonki/target/debug/animus", logString = "/home/krawacik3/Unity/Learn/log.log";
    private string physicsRateString = "50", updateRateString = "100";
    private FileStream logFile;
    public AnimalHandler animalHandler;
    public Terrain terrain;
    
    
    // Start is called before the first frame update
    void Start()
    {
    }

    void OnDestroy()
    {
        consoleAppManager?.Kill();
        logFile?.Close();
    }

    // Update is called once per frame
    void Update()
    {
        if (consoleAppManager != null)
        {
            if (!consoleAppManager.Running)
            {
                initialized = false;
                started = false;
            }
        }
    }

    void StartBackend()
    {
        Debug.Log("Starting backend: " + JsonConvert.SerializeObject("start"));
        logFrontEnd("Starting backend");
        consoleAppManager.Write(JsonConvert.SerializeObject("start"));
    }

    void OnProcessExit(object obj, EventArgs args)
    {
        started = false;
        initialized = false;
        logFile?.Close();
    }

    void logFrontEnd(string s)
    {
        byte[] log = Encoding.GetEncoding("UTF-8").GetBytes("FRONT: " + s + Environment.NewLine);
        if (logFile.CanWrite)
        {
            logFile?.WriteAsync(log, 0, log.Length);
        }
    }

    void logBackEnd(object obj, string s)
    {
        byte[] log = Encoding.GetEncoding("UTF-8").GetBytes("BACK: " + s + Environment.NewLine);
        if (logFile.CanWrite)
        {
            logFile?.WriteAsync(log, 0, log.Length);
        }
    }

    public void receiveResponse(object obj, string response)
    {
        logFrontEnd("Received: " + response);
        if (response == "Initialized\n")
        {
            initialized = true;
            logFrontEnd("Initialized backend");
        }
        else if (response == "Uninitialized\n")
        {
            //Pass
        }
        else if (response == "Running\n")
        {
            //Pass
        }
        else
        {
            backendResponses.Append(response);
            UpdateAnimals(response);
            Debug.Log("Received: " + response);
        }
    }    
    
    private void OnGUI()
    {
        //Interface properties
        GUI.Box(new Rect(10, 10, 200, 250), "Interface info");
        GUI.Label(new Rect(15, 35, 80, 25), "Filename:");
        fileNameString = GUI.TextField(new Rect(100, 35, 100, 25), fileNameString);
        GUI.Label(new Rect(15, 60, 80, 25), "Log file:");
        logString = GUI.TextField(new Rect(100, 60, 100, 25), logString);
        
        // Rates
        var beforePhysicsRate = physicsRateString;
        var beforeUpdateRate = updateRateString;
        GUI.Label(new Rect(15, 85, 80, 25), "Physix rate:");
        physicsRateString = GUI.TextField(new Rect(100, 85, 100, 25), physicsRateString);
        GUI.Label(new Rect(15, 110, 80, 25), "Update rate:");
        updateRateString = GUI.TextField(new Rect(100, 110, 100, 25), updateRateString);
        // Check if inserted data was correct
        int number;
        if (!int.TryParse(physicsRateString, out number))
        {
            physicsRateString = beforePhysicsRate;
        }
        if (!int.TryParse(updateRateString, out number))
        {
            updateRateString = beforeUpdateRate;
        }
        
        if (GUI.Button(new Rect(15, 160, 190, 40), "Launch"))
        {
            consoleAppManager = new ConsoleAppManager(fileNameString);
            consoleAppManager.StandardTextReceived += receiveResponse;
            consoleAppManager.ErrorTextReceived += logBackEnd;
            consoleAppManager.ProcessExited += OnProcessExit;
            consoleAppManager.ExecuteAsync();
            logFile = File.Create(logString);
            SendSetUp();
            Thread.Sleep(500);
            StartBackend();
        }

        if (GUI.Button(new Rect(15, 210, 190, 40), "Kill"))
        {
            consoleAppManager?.Kill();
        }
    }

    class JsonSignInMessage
    {
        public JsonSignIn sign_in { get; set; }
    }

    private void SendSetUp()
    {
        logFrontEnd("Sending sign_in...");
        //Fill setup
        JsonSignInMessage msg = new JsonSignInMessage()
        {
            sign_in = new JsonSignIn()
            {
                physix_rate = int.Parse(physicsRateString),
                update_rate = int.Parse(updateRateString),
                world = GetWorld()
            }
        };

        var jsonMsg = JsonConvert.SerializeObject(msg);
        if (consoleAppManager.Running)
        {
            logFrontEnd(jsonMsg);
            consoleAppManager.Write(jsonMsg);
        }
    }

    private JsonWorld GetWorld()
    {
        JsonWorld world = new JsonWorld()
        {
            animals = GetAnimals(),
            map = GetMap(),
        };
        return world;
    }

    private JsonPosition[] GetTrees()
    {
        List<JsonPosition> positions = new List<JsonPosition>();
        foreach (var treeInstance in terrain.terrainData.treeInstances)
        {
            double x = Math.Round(treeInstance.position.x * terrain.terrainData.size.x, 1, MidpointRounding.ToEven);
            double y = Math.Round(treeInstance.position.x * terrain.terrainData.size.x, 1, MidpointRounding.ToEven);
            positions.Add(new JsonPosition(){
                x = x,
                y = y
            });
        }

        return positions.ToArray();
    }

    private JsonMap GetMap()
    {
        JsonMap map = new JsonMap()
        {
            grass = new JsonPosition[]{new JsonPosition(){}},
            trees = GetTrees(),
            water = new JsonPosition[]{new JsonPosition(){}}
        };
        return map;
    }

    public string getAnimalKind(int id)
    {
        switch (id)
        {
            case 0:
                return "Fox";
            case 1:
                return "Deer";
            case 2:
                return "Wolf";
            case 3:
                return "Moose";
        }

        return "";
    }


    private JsonAnimal[] GetAnimals()
    {
        List<JsonAnimal> animalList = new List<JsonAnimal>();
        
        foreach (var animal in animalHandler.animals)
        {
            var position = animal.GetPosition();
            var jsonAnimal = new JsonAnimal()
            {
                id = animal.ID,
                kind = getAnimalKind(animal.PrefabType),
                position = new JsonPosition()
                {
                    x = position.x,
                    y = position.z,
                },
                velocity = new JsonPosition()
                {
                    x = 0,
                    y = 0
                }
            };
            animalList.Add(jsonAnimal);
        }

        return animalList.ToArray();
    }

    private void UpdateAnimals(string rawJson)
    {
        try
        {
            var update = JsonConvert.DeserializeObject<JsonUpdateAnimals>(rawJson);
            foreach (var jsonAnimal in update.animals)
            {
                animalHandler.UpdateAnimal(jsonAnimal.id, ConvertFromJson(jsonAnimal.position), ConvertFromJson(jsonAnimal.velocity));
            }
        }
        catch (Exception ex)
        {
            Debug.Log(ex.Message);
        }
    }

    public Vector3 ConvertFromJson(JsonPosition position)
    {
        return new Vector3((float)position.x, 0, (float)position.y);
    }
}
