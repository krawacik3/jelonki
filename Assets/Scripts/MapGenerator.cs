﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using static MapData;
using Random = System.Random;

public class MapGenerator : MonoBehaviour
{
    private TreeInstance getTreeInstance1(double x, double z)
    {
        return new TreeInstance
        {
            position = new Vector3((float)x, 0, (float)z),
            heightScale = 1,
            widthScale = 1,
            prototypeIndex = 0,
            color = Color.white,
            rotation = 0,
            lightmapColor = Color.white
        };
    }

private TreeInstance tree2 = new TreeInstance
    {
        position = new Vector3(0, 0, 0),
        heightScale = 1,
        widthScale = 1,
        prototypeIndex = 1,
        color = Color.white,
        rotation = 0,
        lightmapColor = Color.white
    };

    MapData mapData;
    public List<TreeInstance> instances = new List<TreeInstance>();
    private void Start()
    {
        //If terrain is already generated, fuck that and just return
        // return;
        Terrain terrain = GetComponent<Terrain>();
        mapData = gameObject.GetComponent<MapData>();
        //Workaround for storing tree prototypes
        var terrainPrototype = terrain.terrainData.treePrototypes;
        //Save previous prototypes in new data
        // terrain.terrainData.treePrototypes = terrainPrototype;
        //Generate new data
        mapData.generateTerrain();

        // terrain.terrainData.heightmapResolution = 257;
        terrain.terrainData.size = new Vector3(mapData.width, 100, mapData.height);
        // terrain.terrainData.size = new Vector3(mapData.width, 1500, mapData.height);
        terrain.terrainData.SetHeights(0, 0, mapData.heights);
        
        //temp
        int deepForestCount = 0;
        int lightForestCount = 0;
        int fieldCount = 0;
        int emptyCount = 0;

        Random random = new Random();

        for (int z = 0; z < mapData.height; ++z)
        {
            for (int x = 0; x < mapData.width; ++x)
            {
                Biome biome = mapData.BiomeAt(x, z);
                float probability = mapData.ProbabilityOfTreeAt(x, z);
                if (random.NextDouble() < probability)
                {
                    double xPos = ((double) x + random.NextDouble()) / mapData.width;
                    double zPos = ((double) z + random.NextDouble()) / mapData.height;
                    TreeInstance instance = getTreeInstance1(xPos, zPos);
                    instances.Add(instance);
                }
                //Statistics
                switch (biome)
                {
                    case Biome.DeepForest:
                        ++deepForestCount;
                        break;
                    case Biome.LightForest:
                        ++lightForestCount;
                        break;
                    case Biome.Field:
                        ++fieldCount;
                        break;
                    case Biome.Empty:
                        ++emptyCount;
                        break;
                }
            }
        }

        terrain.terrainData.SetTreeInstances(instances.ToArray(), true);
        Debug.Log(deepForestCount + ", " + lightForestCount + ", " + fieldCount + ", " + emptyCount + " instances:" + terrain.terrainData.treeInstances.Length);
        terrain.Flush();
    }
}
