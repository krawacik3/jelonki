﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public Transform transform;
    public Camera camera;
    public Terrain terrain;

    public float moveSpeed = 2f;
    public float panSpeedVert = 1f;
    public float panSpeedHor = -1f;
    public float heightAboveGround = 40f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        const int MIDDLE_BUTTON = 2;
        const int LEFT_BUTTON = 0;
        //Start of panning mode
        if (Input.GetMouseButtonDown(LEFT_BUTTON))
        {
            // transform.Rotate(0, 1, 0, Space.Self);
        }
        
        //Panning mode
        if (Input.GetMouseButton(LEFT_BUTTON))
        {
            float newAngleVert = Input.GetAxisRaw("Mouse Y") * panSpeedVert;
            float newAngleHor = Input.GetAxisRaw("Mouse X") * panSpeedHor;
            transform.Rotate(newAngleVert, 0, 0, Space.Self);
            transform.Rotate(0, newAngleHor, 0, Space.World);
        }

        //Moving mode
        if (Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0)
        {
            Vector3 forward = transform.forward.normalized;
            forward.Normalize();
            Vector3 right = transform.right.normalized;
            transform.position += forward * (moveSpeed * Input.GetAxisRaw("Vertical"))
                                  + right * (moveSpeed * Input.GetAxisRaw("Horizontal"));
            // Height of camera
            float height = terrain.SampleHeight(transform.position) + heightAboveGround;
            transform.position = new Vector3(transform.position.x, height, transform.position.z);
        }
    }
}
