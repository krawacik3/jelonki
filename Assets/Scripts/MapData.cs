﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapData : MonoBehaviour
{
    public readonly float[] scales = {1, 10, 100};
    public readonly float[] amplitudes = {0.5f, 0.03f, 0.0005f};
    public readonly int width = 257;
    public readonly int height = 257;
    public float perlinNoiseOffset = -50f;
    
    public float[,] heights;// = GenerateHeightsPerlin();
    public float[,] biomeHeights;// = GenerateHeightsPerlin(500f);
    
    public enum Biome
    {
        DeepForest,
        LightForest,
        Field,
        Empty
    }
    
    //Tresholds used to determine biomes
    private float deepForestTreshold = 1f;
    private float lightForestTreshold = 1f;
    private float fieldTreshold = 1f;

    public MapData()
    {
        generateTerrain();
    }

    public void generateTerrain()
    {
        heights = GenerateHeightsPerlin(perlinNoiseOffset);
        biomeHeights = GenerateHeightsPerlin(2 * perlinNoiseOffset);
        calculateCriteriaForBiomeHeights();
    }
    
    float[,] GenerateHeightsPerlin(float offset = 0f)
    {
        float [,] heights = new float[width, height];
        for (int index = 0; index < scales.Length && index < amplitudes.Length; ++index)
        {
            for (int x = 0; x < width; ++x)
            {
                for (int z = 0; z < height; ++z)
                {
                    heights[x, z] += CalculatePerlin(x, z, scales[index], amplitudes[index], offset);
                }
            }
        }

        return heights;
    }

    float CalculatePerlin(int x, int z, float scale, float amplitude, float offset)
    {
        float xPos = (float)x / width * scale + offset;
        float zPos = (float)z / height * scale + offset;
        return Mathf.PerlinNoise(xPos, zPos) * amplitude;
    }

    public float ProbabilityOfTreeAt(int x, int z)
    {
        return ProbabilityOfTreeFromBiome(BiomeAt(x, z));
    }

    public Biome BiomeAt(int x, int z)
    {
        return GetBiomeFromHeight(biomeHeights[x, z]);
    }
    
    static float ProbabilityOfTreeFromBiome(Biome biome)
    {
        switch (biome)
        {
            case Biome.DeepForest:
                return 0.05f;
            case Biome.LightForest:
                return 0.01f;
            case Biome.Field:
                return 0.005f;
            case Biome.Empty:
                return 0f;
        }
        throw new Exception("Biome not covered");
    }
    
    Biome GetBiomeFromHeight(float height)
    {
        if (height > deepForestTreshold)
        {
            return Biome.DeepForest;
        }
        if (height > lightForestTreshold)
        {
            return Biome.LightForest;
        }
        if (height > fieldTreshold)
        {
            return Biome.Field;
        }
        return Biome.Empty;
    }

    private void calculateCriteriaForBiomeHeights()
    {
        float[] tempFloat = new float[width * height];
        Buffer.BlockCopy(biomeHeights, 0, tempFloat, 0, biomeHeights.Length);
        Array.Sort(tempFloat);
        int lastZero = Array.LastIndexOf(tempFloat, 0);
        //15% of forest is deep forest
        deepForestTreshold = tempFloat[lastZero + GetTreshold(tempFloat.Length - lastZero, 0.15f)];
        //20% of forest is deep forest
        lightForestTreshold = tempFloat[lastZero + GetTreshold(tempFloat.Length - lastZero,0.35f)];
        //30% of forest is deep forest
        fieldTreshold = tempFloat[lastZero + GetTreshold(tempFloat.Length - lastZero, 0.65f)];
        
        Debug.Log("deepForestTreshold " + deepForestTreshold);
        Debug.Log("lightForestTreshold " + lightForestTreshold);
        Debug.Log("fieldTreshold " + fieldTreshold);
    }

    private int GetTreshold(int length, float treshold)
    {
        return (int)((float)length * (1f - treshold));
    }
}
