﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using NUnit.Framework;
using UnityEngine;

namespace Tests
{
    [TestFixture]
    public class InterfaceFixture
    {
        Interface interf = new Interface();
        // A Test behaves as an ordinary method
        [Test]
        public void SanityCheck()
        {
            Assert.Equals(1, 1);
        }

        [Test]
        public void TestAnimalKindValid()
        {
            Assert.Equals(interf.getAnimalKind(0), "Fox");
        }

        [Test]
        public void TestAnimalKindInvalid()
        {
            Assert.Equals(interf.getAnimalKind(9999999), "");
        }
        
        
        [Test]
        public void TestConversionOfPosition()
        {
            var posX = 1234.56;
            var posY = -543.21;
            JsonPosition pos = new JsonPosition()
            {
                x = posX,
                y = posY
            };
            
            Vector3 position3d = interf.ConvertFromJson(pos);
            // ConvertFromJson is meant to convert 2d position in a terrain to 3D interpretation
            // where x, y maps to x, z (y in Vector3 is vertical direction)
            Assert.Equals(position3d.x, posX);
            Assert.Equals(position3d.y, 0);
            Assert.Equals(position3d.z, posY);
        }

        [Test]
        public void TestReceiveResponseInitialized()
        {
            // Make sure that nothing in this fixture modified backend responses
            Assert.Equals(interf.backendResponses.Length, 0);
            interf.receiveResponse(null, "Initialized\n");
            
            // Interface should initialize backend
            Assert.Equals(interf.backendResponses.Length, 0);
            Assert.True((bool?) interf.initialized);
            
        }

        [Test]
        public void TestReceiveResponseMessage()
        {
            // Make sure that nothing in this fixture modified backend responses
            Assert.Equals(interf.backendResponses.Length, 0);
            interf.receiveResponse(null, "Some diffr3nt message\n");

            Assert.Equals(interf.backendResponses.Length, 1);
        }
    }
}
