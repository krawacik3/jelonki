﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalController : MonoBehaviour
{
    private CharacterController characterController;

    private Animator animator;

    public float moveSpeed = 0.01f;
    public Vector3 sampleDestination = Vector3.one;
    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        walkTowards(sampleDestination);
    }

    public void walkTowards(Vector3 destination)
    {
        //Modify y coord
        destination.y = characterController.transform.position.y;
        // If animal reached destination
        if (Vector3.Distance(characterController.transform.position, destination) < 0.01f)
        {
            animator.SetTrigger("stopWalking");
            return;
        }
        animator.SetTrigger("startWalking");
        characterController.transform.LookAt(destination);
        characterController.Move((destination - characterController.transform.position).normalized * moveSpeed * Time.deltaTime);
    }
}
