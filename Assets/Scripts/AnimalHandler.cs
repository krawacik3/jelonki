﻿using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEditor;
using UnityEngine;
using UnityEngine.Assertions;

using Random = System.Random;

public class Animal
{
    public Animal(int prefabType, int id, GameObject model_)
    {
        velocity = Vector3.zero;
        PrefabType = prefabType;
        ID = id;
        model = model_;
        animalController = model.GetComponent<AnimalController>();
    }
    public Vector3 velocity;
    public Vector3 destination;
    public readonly int PrefabType;
    public readonly int ID;
    private GameObject model;
    private AnimalController animalController;

    public void UpdateDestination(Vector3 newDestination)
    {
        destination = newDestination;
        animalController.sampleDestination = destination;
    }

    public Vector3 GetPosition()
    {
        return model.transform.position;
    }
}

public class AnimalHandler : MonoBehaviour
{
    public GameObject[] animalPrefabs;

    public List<Animal> animals = new List<Animal>();

    public int spawnStart = 100, spawnEnd = 200;

    public int numOfAnimals = 50;

    private Random random;
    // Start is called before the first frame update
    void Start()
    {
        Assert.IsTrue(spawnEnd - spawnStart > 0, "Spawn window should is invalid");
        random = new Random();
        for (int animalIdx = 0; animalIdx < numOfAnimals; ++animalIdx)
        {
            int prefabIdx = animalIdx % animalPrefabs.Length;
            Vector3 positionInSpace = GenerateRandomPosition(spawnStart, spawnEnd);
            Animal animal = new Animal(prefabIdx, animalIdx,
                Instantiate(animalPrefabs[prefabIdx], positionInSpace, Quaternion.identity));
            animals.Add(animal);
        }
    }

    public void UpdateAnimal(int ID, Vector3 position, Vector3 velocity)
    {
        // velocity should be scaled by the 
        var newDestination = position + velocity;
        var animal = animals.Find(x => x.ID == ID);
        animal.UpdateDestination(newDestination);
    }

    private Vector3 GenerateRandomPosition(int min, int max)
    {
        return new Vector3(random.Next(min, max), 200, random.Next(min, max));
    }
    
    // Update is called once per frame
    void Update()
    {

    }
}
